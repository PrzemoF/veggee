<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package veggee
 */

get_header(); ?>
<div class="row">
	<div id="primary" class="content-area
		<?php $veggee_home_page_layout = get_theme_mod( 'home_page_layout', 'grid' );
		echo ( empty( $veggee_home_page_layout ) ) ? ' col-md-12' : ' col-lg-9';
		if ( ! empty( $veggee_home_page_layout ) && ! is_active_sidebar( 'sidebar-1' ) ) :
			echo ' col-lg-push-2';
		endif; ?>
		" style="background-image: url('<?php echo esc_attr ( get_theme_mod( 'image_404', '' )) ?>');
			background-position: center;
			background-repeat: no-repeat;
			background-size: 70%;" ><!-- content-area -->
		<div class="veggee-page-intro">
			<h1><?php echo esc_html__( 'Error 404', 'veggee' ); ?></h1>
		</div>

		<main id="main" class="site-main" role="main" ); >
			<div class="veggee-404">
				<h1>404</h1>
			</div>
			<div class="page-content">
				<p><?php printf( '<span class="lead">%s</span><br/>%s',
						esc_html__( 'It looks like nothing was found at this location.', 'veggee' ),
						esc_html__( 'Maybe try a search?', 'veggee' ) ); ?>
				</p>
				<?php get_search_form(); ?>
			</div><!-- .page-content -->
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php if ( ! empty( $veggee_home_page_layout ) ) { get_sidebar(); } ?>
</div><!-- .row -->
<?php get_footer(); ?>
