<?php
/**
 * Template part for displaying header slider.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package veggee
 */

$veggee_sticky_posts = get_option( 'sticky_posts' );
if ( ! empty( $veggee_sticky_posts ) ) :

	$veggee_args = array(
		'posts_per_page'   => 10,
		'post__in'          => $veggee_sticky_posts,
		'post_type'        => 'post',
	);

	$veggee_the_slider = new WP_Query( $veggee_args ); ?>

		<div id="slider-container">
				<div class="featured-posts">
					<div class="veggee-featured-slider">
					<?php if ( $veggee_the_slider->have_posts() ) : ?>
						<?php
						while ( $veggee_the_slider->have_posts() ) :
							$veggee_the_slider->the_post();
							?>
								<div class="veggee-featured-slider-wrapper">
									<?php if ( has_post_thumbnail() ) : ?>
									 <div class="featured-image" style="background:#000 url(<?php the_post_thumbnail_url( get_theme_mod( 'home_page_slider_img_size', 'full' ) ); ?>) no-repeat center;background-size: cover;">
										<div class="veggee-featured-slider-title-wrapper container">
											<div class="row">
											 <div class="veggee-featured-slider-title col-sm-12 col-lg-6">
														<span class="featured-category">
														<?php the_category( __( '<span> &#124; </span>', 'veggee' ) ); ?>
														</span>
														<h2 class="veggee-featured-slider-header"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
											 </div>
										 </div>
										<?php echo veggee_post_format_icon( $veggee_the_slider->ID ); // WPCS: XSS OK. ?>
										</div>
										</div>
									<?php else : ?>
									<div class="no-featured-image">
										<div class="veggee-featured-slider-title-wrapper container">
											<div class="row">
											<div class="veggee-featured-slider-title col-sm-12 col-lg-6">
												<span class="featured-category">
												<?php the_category( __( '<span> &#124; </span>', 'veggee' ) ); ?>
												</span>
												<h2 class="veggee-featured-slider-header"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
											</div>
											</div>
											</div>
									</div>
									<?php endif; ?>
								</div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
					</div>
				</div>
		</div>
<?php endif; ?>
