<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veggee
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_attr_e( 'Skip to content', 'veggee' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
				<nav id="top-navigation" class="navbar-navigation" role="navigation">

					<button id="left-navbar-toggle" class="menu-toggle" aria-controls="left-sidebar" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Menu', 'veggee' ); ?></span><i class="fas fa-bars"></i></button>
					<?php
					if ( function_exists( 'wc_get_cart_url' ) ) :
						?>
						<a class="btn veggee-cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_html_e( 'Cart', 'veggee' ); ?>"><?php esc_html_e( 'Cart', 'veggee' ); ?>(<span class="veggee-cart-content-counts"><?php echo esc_html( wc()->cart->get_cart_contents_count() ); ?></span>)</a>
						<?php
						endif;
					?>
					<button id="navbar-search-toggle" class="search-toggle" aria-controls="search-panel" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Search', 'veggee' ); ?></span><i class="fa fa-search"></i></button>
					<div id="search-panel" class="veggee-search-panel">
						<button class="veggee-search-panel-close" title="<?php esc_attr_e( 'Close', 'veggee' ); ?>"><svg><path d="M18.984 6.422l-5.578 5.578 5.578 5.578-1.406 1.406-5.578-5.578-5.578 5.578-1.406-1.406 5.578-5.578-5.578-5.578 1.406-1.406 5.578 5.578 5.578-5.578z"></path></svg></button>
						<?php get_search_form(); ?>
					</div>

					<div class="menu-logo">
						<?php
						if ( has_custom_logo() ) :
							the_custom_logo();
						else :
							?>
						<p class="menu-blogname"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" ><?php bloginfo( 'name' ); ?></a></p>
						<?php endif; ?>
					</div>
					<div class="top-menu-background">
						<img src="<?php echo (esc_html( get_theme_mod( 'top_menu_background_image', '' ) ) ) ?>" alt="top menu background">
					</div>
						<?php
						if ( has_nav_menu( 'social' ) ) {
							wp_nav_menu(
								array(
									'theme_location' => 'social',
									'menu_id' => 'social-menu',
									'container_class' => 'social-menu-container',
									'depth'           => 1,
									'link_before'     => '<span class="screen-reader-text">',
									'link_after'      => '</span>',
								)
							);
						}
						wp_nav_menu(
							array(
								'theme_location' => 'top',
								'menu_id' => 'top-menu',
								'container_id' => 'top-menu-container',
								'item_spacing'    => 'discard',
							)
						);
						?>
		</nav><!-- #site-navigation -->

		<?php if ( is_front_page() && is_home() ) : ?>
		<div class="site-branding">
					<?php if ( get_header_image() ) : ?>
						<h1 class="site-baner">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<img src="<?php header_image(); ?>" alt="<?php bloginfo( 'name' ); ?>" >
							</a>
						</h1>
						<h1 class="site-title"><?php bloginfo( 'name' ); ?>
						</h1>
						<h2 class="site-description"><?php bloginfo( 'description' ); ?>
						</h2>
					<?php endif; ?>
					<h1 class="secondary-baner">
						<?php if ( get_theme_mod ( 'secondary_baner', '' ) ) : ?>
							<?php echo "<img src=", esc_url( get_theme_mod ( 'secondary_baner', '' )), " class=\"secondary-baner\" alt=\"secondary baner\">"; ?>
						<?php endif; ?>
					</h1>
		</div><!-- .site-branding -->
		<?php endif; ?>

	</header><!-- #masthead -->

	<?php if ( is_front_page() && is_home() ) : ?>
		<?php get_template_part( 'template-parts/content', 'home-slider' ); ?>
	<?php endif; ?>

	<div id="content" class="site-content container">
