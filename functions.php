<?php
/**
 * Veggee functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package veggee
 */

if ( ! function_exists( 'veggee_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function veggee_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on veggee, use a find and replace
		 * to change 'veggee' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'veggee', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in three location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Left Menu', 'veggee' ),
				'top' => esc_html__( 'Top Menu', 'veggee' ),
				'social' => esc_html__( 'Social Menu', 'veggee' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support(
			'post-formats',
			array(
				'audio',
				'video',
				'gallery',
			)
		);

		// Enable support for Custom Header.
		$defaults = array(
			'width'                  => 1200,
			'height'                 => 300,
			'flex-height'            => true,
			'flex-width'             => true,
			'uploads'                => true,
			'header-text'            => true,
			'default-text-color'     => '#FFFFFF',
		);
		add_theme_support( 'custom-header', $defaults );

		// Enable support for Site Logo.
		add_theme_support( 'custom-logo' );

		// Enable support for WooCommerce Shopping Cart.
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

endif; // End of veggee_setup.
add_action( 'after_setup_theme', 'veggee_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function veggee_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'veggee_content_width', 738 ); // WPCS: prefix ok.
}

add_action( 'after_setup_theme', 'veggee_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function veggee_widgets_init() {
	register_sidebar(
		array(
			'name' => esc_html__( 'Right Sidebar', 'veggee' ),
			'id' => 'sidebar-1',
			'description' => esc_html__( 'Right Sidebar Widget Area', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);

	register_sidebar(
		array(
			'name' => esc_html__( 'Left Menu (Mobile)', 'veggee' ),
			'id' => 'sidebar-2',
			'description' => esc_html__( 'Mobile Menu Widget Area', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);

	register_sidebar(
		array(
			'name' => esc_html__( 'Top', 'veggee' ),
			'id' => 'top-1',
			'description' => esc_html__( 'Top Widget Area. Above the content - on home and archive pages', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);

	register_sidebar(
		array(
			'name' => esc_html__( 'Bottom', 'veggee' ),
			'id' => 'bottom-1',
			'description' => esc_html__( 'Bottom Widget Area.', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);

	register_sidebar(
		array(
			'name' => esc_html__( 'Footer', 'veggee' ),
			'id' => 'footer-1',
			'description' => esc_html__( 'Footer Widget Area.', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="col-xs-12 col-md-6 col-lg-3 widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);

	register_sidebar(
		array(
			'name' => esc_html__( 'Shop Sidebar', 'veggee' ),
			'id' => 'shop-sidebar-1',
			'description' => esc_html__( 'Shop Sidebar Widget Area.', 'veggee' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h2 class="widget-title"><span>',
			'after_title' => '</span></h2>',
		)
	);
}

add_action( 'widgets_init', 'veggee_widgets_init' );

if ( ! function_exists( 'veggee_fonts_url' ) ) :

	/**
	 * Register Google fonts for Twenty Fifteen.
	 *
	 * @since Twenty Fifteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function veggee_fonts_url() {

		if ( ! get_theme_mod( 'load_google_fonts_from_google', 1 ) ) {
			return get_template_directory_uri() . '/fonts/fonts.css';
		}

		$fonts_url = '';
		$fonts = array();
		$subsets = 'latin';

		/*
		 * Translators: If there are characters in your language that are not supported
		 * by Amiri, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Amiri font: on or off', 'veggee' ) ) {
			$fonts[] = 'Amiri:700,400,400italic';
		}

		/*
		 * Translators: If there are characters in your language that are not supported
		 * by Work Sans, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Work Sans font: on or off', 'veggee' ) ) {
			$fonts[] = 'Work Sans:400';
		}

		/*
		 * Translators: To add an additional character subset specific to your language,
		 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
		 */
		$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'veggee' );

		if ( 'cyrillic' === $subset ) {
			$subsets .= ',cyrillic,cyrillic-ext';
		} elseif ( 'greek' === $subset ) {
			$subsets .= ',greek,greek-ext';
		} elseif ( 'devanagari' === $subset ) {
			$subsets .= ',devanagari';
		} elseif ( 'vietnamese' === $subset ) {
			$subsets .= ',vietnamese';
		}

		if ( $fonts ) {
			$fonts_url = esc_url(
				add_query_arg(
					array(
						'family' => urlencode( implode( '|', $fonts ) ),
						'subset' => urlencode( $subsets ),
					),
					'https://fonts.googleapis.com/css'
				)
			);
		}

		return $fonts_url;
	}

endif;

/**
 * Enqueue scripts and styles.
 */
function veggee_scripts() {

	$veggee_theme_info = wp_get_theme();
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'veggee-fonts', veggee_fonts_url(), array(), $veggee_theme_info->get( 'Version' ) );

	wp_enqueue_style( 'veggee-style', get_stylesheet_uri(), array(), $veggee_theme_info->get( 'Version' ) );

	wp_enqueue_script( 'slick', get_template_directory_uri() . '/slick/slick.min.js', array( 'jquery' ), '20150828', true );

	if ( ! is_singular() && ! is_404() && have_posts() ) {
		wp_enqueue_script( 'jquery-infinite-scroll', get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array( 'jquery', 'masonry' ), '2.1.0', true );
	}

	if ( get_theme_mod( 'sticky_sidebar', 1 ) && is_active_sidebar( 'sidebar-1' ) ) {
		wp_enqueue_script( 'theia-sticky-sidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.min.js', array( 'jquery' ), '1.2.2', true );
	}

	wp_enqueue_script( 'veggee-scripts', get_template_directory_uri() . '/js/veggee.min.js', array( 'jquery', 'jquery-effects-core', 'jquery-effects-slide' ), $veggee_theme_info->get( 'Version' ), true );

	wp_enqueue_script( 'veggee-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.min.js', array(), '20130115', true );

	// Preparing to pass variables to js -> load more posts via ajax call.
	global $wp_query;
	$veggee_ajax_max_pages = $wp_query->max_num_pages;
	$veggee_ajax_paged = ( get_query_var( 'paged' ) > 1 ) ? get_query_var( 'paged' ) : 1;
	$veggee_pagination = get_theme_mod( 'pagination', 'infinite' );
	$veggee_home_page_slider_play_speed = get_theme_mod( 'home_page_slider_play_speed', 4000 );
	$veggee_home_page_slider_autoplay = ( 0 == $veggee_home_page_slider_play_speed ) ? false : true;

	// Passing theme options to veggee.js.
	wp_localize_script(
		'veggee-scripts',
		'veggee',
		array(
			'home_page_slider_img_number' => get_theme_mod( 'home_page_slider_img_number', 2 ),
			'home_page_slider_play_speed' => $veggee_home_page_slider_play_speed,
			'home_page_slider_autoplay' => $veggee_home_page_slider_autoplay,
			'loadMoreText' => esc_html__( 'Load more posts', 'veggee' ),
			'loadingText' => '',
			'noMorePostsText' => esc_html__( 'No More Posts', 'veggee' ),
			'expandText' => esc_html__( 'Expand', 'veggee' ),
			'closeText' => esc_html__( 'Close', 'veggee' ),
			'LoginButtonText' => esc_html__( 'Login', 'veggee' ),
			'RegisterButtonText' => esc_html__( 'Create An Account', 'veggee' ),
			'startPage' => $veggee_ajax_paged,
			'maxPages' => $veggee_ajax_max_pages,
			'nextLink' => next_posts( $veggee_ajax_max_pages, false ),
			'pagination' => $veggee_pagination,
			'getTemplateDirectoryUri' => esc_url( get_template_directory_uri() ),
			'months' => veggee_months(),
			'days' => veggee_days(),
		)
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'veggee_scripts' );

/**
* Font Awesome
**/
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() {
	wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Hybrid Media Grabber for getting media from posts.
 */
require get_template_directory() . '/inc/class-media-grabber.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Some meta fields for category styling.
 */
require get_template_directory() . '/inc/class-veggee-meta-for-categories.php';

/**
 * Load TGMPA recommended plugins.
 */
require_once get_template_directory() . '/inc/tgmpa-plugins.php';

/**
 * AMP.
 */
require_once get_template_directory() . '/inc/amp.php';

/**
 * Twitter in user personal information
 */
add_action( 'show_user_profile', 'crf_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'crf_show_extra_profile_fields' );

function crf_show_extra_profile_fields( $user ) {
	$twitter = get_the_author_meta( 'twitter', $user->ID );
	$facebook = get_the_author_meta( 'facebook', $user->ID );
	$instagram = get_the_author_meta( 'instagram', $user->ID );
	$pinterest = get_the_author_meta( 'pinterest', $user->ID );
	?>
	<h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="twitter"><?php esc_html_e( 'Twitter ', 'crf' ); ?><i class="fab fa-twitter"></i></label></th>
			<td>
				<input type="url" id="twitter" name="twitter"
				value="<?php echo $twitter; ?>"
				class="regular-text" />
			</td>
		</tr>
		<tr>
			<th><label for="facebook"><?php esc_html_e( 'Facebook ', 'crf' ); ?><i class="fab fa-facebook-f"></i></label></th>
			<td>
				<input type="url" id="facebook" name="facebook"
				value="<?php echo $facebook; ?>"
				class="regular-text" />
			</td>
		</tr>
		<tr>
			<th><label for="instagram"><?php esc_html_e( 'Instagram ', 'crf' ); ?><i class="fab fa-instagram"></i></label></th>
			<td>
				<input type="url" id="instagram" name="instagram"
				value="<?php echo $instagram; ?>"
				class="regular-text" />
			</td>
		</tr>
		<tr>
			<th><label for="pinterest"><?php esc_html_e( 'Pinterest ', 'crf' ); ?><i class="fab fa-pinterest"></i></label></th>
			<td>
				<input type="url" id="pinterest" name="pinterest"
				value="<?php echo $pinterest; ?>"
				class="regular-text" />
			</td>
		</tr>
	</table>
	<?php
}

add_action( 'user_profile_update_errors', 'crf_user_profile_update_errors', 10, 3 );
function crf_user_profile_update_errors( $errors, $update, $user ) {
	if ( ! empty( $_POST['twitter'] ) ) {
		if ( ! wp_http_validate_url( $_POST['twitter'] ) ) {
			$errors->add( 'twitter_error', __( '<strong>ERROR</strong>: Twitter URL is not valid.', 'crf' ) );
		}
	}
	if ( ! empty( $_POST['facebook'] ) ) {
		if ( ! wp_http_validate_url( $_POST['facebook'] ) ) {
			$errors->add( 'facebook_error', __( '<strong>ERROR</strong>: Facebook URL is not valid.', 'crf' ) );
		}
	}
	if ( ! empty( $_POST['instagram'] ) ) {
		if ( ! wp_http_validate_url( $_POST['instagram'] ) ) {
			$errors->add( 'instagram_error', __( '<strong>ERROR</strong>: Instagram URL is not valid.', 'crf' ) );
		}
	}
	if ( ! empty( $_POST['pinterest'] ) ) {
		if ( ! wp_http_validate_url( $_POST['pinterest'] ) ) {
			$errors->add( 'pinterest_error', __( '<strong>ERROR</strong>: Instagram URL is not valid.', 'crf' ) );
		}
	}
}


add_action( 'personal_options_update', 'crf_update_profile_fields' );
add_action( 'edit_user_profile_update', 'crf_update_profile_fields' );

function crf_update_profile_fields( $user_id ) {
	if ( ! current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}

	if ( ! empty( $_POST['twitter'] ) ) {
		update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	}

	if ( ! empty( $_POST['facebook'] ) ) {
		update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	}

	if ( ! empty( $_POST['instagram'] ) ) {
		update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
	}

	if ( ! empty( $_POST['pinterest'] ) ) {
		update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
	}
}

function filter_search($query) {
    //---- Don't run in admin area
    if(!is_admin()) {
        // Limit search to posts
        if($query->is_main_query() && $query->is_search()) {
            $query->set('post_type', array('post'));
        }
        // Return query
        return $query;
    }
}
add_filter('pre_get_posts', 'filter_search');
