
(function($){
     'use strict';

$(document).ready(function($) {

$('a').on( 'click', function(){
  $(this).blur();
});

   var $veggeeSecondary = $('#secondary');

  if(typeof $.fn.theiaStickySidebar === 'function'){
      $veggeeSecondary.theiaStickySidebar({
        additionalMarginTop: 126 //Number(veggeeCustomScript.fatNavbarHeight)
      });
  }

if (typeof $.fn.slick === 'function'){

var $veggeeFeaturedSliderImage = $('.veggee-featured-slider-wrapper');

$veggeeFeaturedSliderImage.show();
// // Slick slider
$('.veggee-featured-slider').slick({
  infinite: true,
  autoplay: Boolean(veggee.home_page_slider_autoplay),
  autoplaySpeed: Number(veggee.home_page_slider_play_speed),
  speed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
        //variableWidth: false
      }
    }
  ]
});
}

// Top search panelchange image url to placeholder url in xml
var $veggeeSearchPanel = $('.veggee-search-panel');
var $veggeeSearchToggle = $('.search-toggle');

    $veggeeSearchToggle.on( 'click', function(){
        $veggeeSearchPanel.slideToggle(250).css('z-index', '1001');
	$(this).attr('aria-expanded', 'true');
        });
    $('.veggee-search-panel-close').on( 'click', function(){
        $veggeeSearchPanel.slideToggle(250).css('z-index', '');
	$veggeeSearchToggle.attr('aria-expanded', 'false');
        });


//Sticky menu
var $veggeeSiteNavigation = $('#top-navigation');
var $veggeeWpAdminBar = $('#wpadminbar');
var lastScrollTop = 0; //, delta = 5;
    $(window).scroll(function(){
      
      var $veggeeAdminBarHeight = $veggeeWpAdminBar.height();
      var $veggeeAdminBarPosistionValue = $veggeeWpAdminBar.css('position');
      var st = $(this).scrollTop();
      if($veggeeAdminBarHeight > 0 ){
	if($veggeeAdminBarPosistionValue === 'fixed') {
	    if ( st > $veggeeAdminBarHeight ) {
	      $veggeeSiteNavigation.css( 'top', $veggeeAdminBarHeight + 'px' );

	     if (st > $veggeeAdminBarHeight + 70) {
		 $veggeeSiteNavigation.css( 'top', $veggeeAdminBarHeight + 'px' );
	      if (st > lastScrollTop){
		  $veggeeSiteNavigation.slideUp(100);
	      } else {
		  $veggeeSiteNavigation.slideDown(200);
	      }
	     } else {
		 $veggeeSiteNavigation.slideDown(200);
	     }
	   } else {
	     $veggeeSiteNavigation.css( 'top', $veggeeAdminBarHeight + 'px' );
	   }
	}
      } else {
	  $veggeeSiteNavigation.css( 'top', '0' );
	 if (st > 70) {

	  if (st > lastScrollTop){
	      $veggeeSiteNavigation.slideUp(100);
	  } else {
	      $veggeeSiteNavigation.slideDown(200);
	  }
	 } else {
	     $veggeeSiteNavigation.slideDown(200);
	 }
      }
     lastScrollTop = st;
    });

	var button, closeButton, menu, bgmenu;

	button = $( '#left-navbar-toggle' );
        bgmenu = $('.left-sidebar-bg');
        menu = $('#left-sidebar');
        closeButton = $('.left-sidebar-close');
        button.on( 'click', function() {
                            veggeeMenuToggle();
                        });

        closeButton.on( 'click', function() {
                            veggeeMenuToggle();
                        });

        bgmenu.on( 'click', function() {
                            veggeeMenuToggle();
                        });

        var veggeeMenuToggle = function(){

	    if(menu.is(':visible')) {

		button.attr('aria-expanded', 'false');
	    } else {

		button.attr('aria-expanded', 'true');
	    }
            menu.toggle('slide', {direction: 'left' } , 150);
            bgmenu.fadeToggle(500);

        };

    var $veggeeMenuPlusLeft = $('#site-navigation .expand-submenu');

    $veggeeMenuPlusLeft.on( 'click', function(){

	if( $(this).hasClass('submenu-expanded')) {
            $(this).css({'transform': 'none'}).removeClass('submenu-expanded').prop('title', veggee.expandText);
	    $(this).parent().children('ul').hide();
        } else {
            $(this).css({'transform': 'rotate(45deg)'}).addClass('submenu-expanded').prop('title', veggee.closeText);
	    $(this).parent().children('ul').show();
        }

    });

    var $veggeeDate = new Date();
    var $veggeeDay = $veggeeDate.getDate();
    var $veggeeFullYear = $veggeeDate.getFullYear();
    var $veggeeDayOfWeek = veggee.days[$veggeeDate.getDay()];
    var $veggeeMonth = veggee.months[$veggeeDate.getMonth()];

    var $veggeeFullDate = $veggeeDayOfWeek + ', ' + $veggeeMonth + ' ' + $veggeeDay + ', ' + $veggeeFullYear;

    $('#today-date').append($veggeeFullDate);

  $('.posts-navigation').hide();

  var $veggeeContainer = $('.masonry-container');

  if (typeof Masonry === 'function'){

    if ( $('body').hasClass('woocommerce') && $('ul').hasClass('products') ) {
      var $veggeeWooProductsContainer = $('ul.products');
      imagesLoaded( document.querySelector('ul.products'), function() {
        $(function(){
            $veggeeWooProductsContainer.masonry({
              itemSelector: 'li.product'
            });
        });
      });
    } else {
      imagesLoaded( document.querySelector('.masonry-container'), function() {
        $(function(){
            $veggeeContainer.masonry({
              itemSelector: '.masonry'
            });
        });
      });
    }
  }
if ( veggee.pagination === 'infinite' ) {
  if (typeof $.infinitescroll === 'function'){

    $veggeeContainer.infinitescroll({

        navSelector  : '.posts-navigation', // selector for the paged navigation (it will be hidden)
        nextSelector : '.nav-previous > a', // selector for the NEXT link (to page 2)
        itemSelector : '.masonry', // selector for all items you'll retrieve
        loading: {
                finished: undefined,
                finishedMsg: '',
                img: veggee.getTemplateDirectoryUri + '/slick/ajax-loader.gif',
                msg: null,
                msgText: veggee.loadingText,
                selector: null,
                speed: 'fast',
                start: undefined
            }

        }, function( newElements ) {
            // hide new items while they are loading
            var $newElems = $( newElements ).css({ opacity: 0 });
            // ensure that images load before adding to masonry layout
            $newElems.imagesLoaded(function(){
              // show elems now they're ready
              $newElems.animate({ opacity: 1 });
              $veggeeContainer.masonry( 'appended', $newElems, true );
            });
        }
    );
  }
}

if( veggee.pagination === 'ajax' ) {
       // The number of the next page to load (/page/x/).
	var pageNum = parseInt(veggee.startPage, 10) + 1;

	// The maximum number of pages the current query can return.
	var max = parseInt(veggee.maxPages, 10);

	// The link of the next page of posts.
	var nextLink = veggee.nextLink;

/**
 * Replace the traditional navigation with our own,
 * but only if there is at least one page of new posts to load.
 */
    if(pageNum <= max) {
	// Insert the "More Posts" link.
	$('#primary:not(.woocommerce)').append('<p id="veggee-load-more"><a class="btn btn-secondary" href="#">' + veggee.loadMoreText + '</a></p>');

	// Remove the traditional navigation.
	$('.navigation').remove();
    }
 /*
 * Load new posts when the link is clicked.
 */
var $veggeeLoadMore = $('#veggee-load-more a');

$veggeeLoadMore.on( 'click', function() {

        // Are there more posts to load?
	if(pageNum <= max) {

                $(this).css('opacity', '0').css('transition', '0');

                $.ajax({
                    url: nextLink,
                    cache: false,
                    dataType: 'html',
                    success: function(response) {

                var result = $(response).find('.masonry');

		if (typeof Masonry === 'function'){

                    result = $( result ).css({ opacity: 0 });

                    $veggeeContainer.append( result ).imagesLoaded(function(){
                        result.animate({ opacity: 1 });
                        $veggeeContainer.masonry( 'appended', result, true );
                            if(typeof $.fn.theiaStickySidebar === 'function'){
                                $veggeeSecondary.theiaStickySidebar({
                                  additionalMarginTop: 105
                                });
                            }
                        pageNum++;
                        nextLink = nextLink.replace(/\/page\/[0-9]*/, '/page/'+ pageNum).replace(/paged=[0-9]*/, 'paged='+ pageNum);
                            if(pageNum <= max) {
                                    $veggeeLoadMore.css('opacity', '1').text( veggee.loadMoreText );
                            } else {
                                    $veggeeLoadMore.css('opacity', '1').text( veggee.noMorePostsText );
                            }
                    });
                }


        }
        });
        } else {
			$veggeeLoadMore.append('');
		}
                return false;

});
}

$veggeeContainer.on( 'click', '.veggee-jp-sharing-toggle', function(){
  $(this).siblings('.comments-link').toggle();
  $(this).toggle().siblings('.veggee-jp-sharing').toggle('slide', {direction: 'right' } , 400, 'linear').siblings('.veggee-jp-sharing-close').toggle();
  $(this).siblings('.veggee-jp-sharing-close').toggle();
});

$veggeeContainer.on( 'click', '.veggee-jp-sharing-close', function(){
  $(this).toggle().siblings('.veggee-jp-sharing').toggle('slide', {direction: 'right' } , 400, 'linear');
  $(this).siblings('.veggee-jp-sharing-toggle').toggle();
  $(this).siblings('.comments-link').fadeToggle(1000);
});

});
})(jQuery);
