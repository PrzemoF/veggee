<?php
/**
 * The template for displaying all author posts.
 *
 * @package veggee
 */

get_header(); ?>

<div class="row">
	<div id="primary" class="content-area
		<?php $veggee_home_page_layout = get_theme_mod( 'home_page_layout', 'classic' );
			echo ( empty( $veggee_home_page_layout ) ) ? ' col-md-12' : ' col-lg-9';
			if ( ! empty( $veggee_home_page_layout ) && ! is_active_sidebar( 'sidebar-1' ) ) :
				echo ' col-lg-push-2';
			endif;
		?>
		 ">
		<div class="row about-author">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 author-avatar-wrapper">
			<div class="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'ID' ), 360 ); ?>
			</div>
			<!--?php if ( class_exists( 'UserSocialProfiles' ) ) : ?-->
			<div class="veggee-author-social-icons">
				<?php
				$veggee_user_url = get_the_author_meta( 'user_url' );
				$veggee_twitter_url = get_the_author_meta( 'twitter' );
				$veggee_facebook_url = get_the_author_meta( 'facebook' );
				$veggee_instagram_url = get_the_author_meta( 'instagram' );
				$veggee_pinterest_url = get_the_author_meta( 'pinterest' );
				?>

				<?php if ( ! empty( $veggee_user_url ) ) : ?>
					<a href="<?php the_author_meta( 'user_url' ); ?>" title="<?php esc_attr_e( 'Website', 'veggee' ); ?>">
						<span class="screen-reader-text"><?php esc_html_e( 'Website', 'veggee' ); ?></span>
						<i class="fas fa-home"></i>
					</a>
				<?php endif; ?>
				<?php if ( ! empty( $veggee_twitter_url ) ) : ?>
					<a href="<?php the_author_meta( 'twitter' ); ?>" title="<?php esc_attr_e( 'Twitter', 'veggee' ); ?>">
						<span class="screen-reader-text"><?php esc_html_e( 'Twitter', 'veggee' ); ?>
						</span><i class="fab fa-twitter"> </i>
					</a>
				<?php endif; ?>
				<?php if ( ! empty( $veggee_facebook_url ) ) : ?>
					<a href="<?php the_author_meta( 'facebook' ); ?>" title="<?php esc_attr_e( 'Facebook', 'veggee' ); ?>">
						<span class="screen-reader-text"><?php esc_html_e( 'Facebook', 'veggee' ); ?>
						</span><i class="fab fa-facebook-f"> </i>
					</a>
				<?php endif; ?>
				<?php if ( ! empty( $veggee_instagram_url ) ) : ?>
					<a href="<?php the_author_meta( 'instagram' ); ?>" title="<?php esc_attr_e( 'Instagram', 'veggee' ); ?>">
						<span class="screen-reader-text"><?php esc_html_e( 'Instagram', 'veggee' ); ?>
						</span><i class="fab fa-instagram"> </i>
					</a>
				<?php endif; ?>
				<?php if ( ! empty( $veggee_pinterest_url ) ) : ?>
					<a href="<?php the_author_meta( 'pinterest' ); ?>" title="<?php esc_attr_e( 'Pinterest', 'veggee' ); ?>">
						<span class="screen-reader-text"><?php esc_html_e( 'Pinterest', 'veggee' ); ?>
						</span><i class="fab fa-pinterest"> </i>
					</a>
				<?php endif; ?>
			</div>
			<!--?php endif; ?-->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
				<h3><?php the_author_meta( 'display_name' ); ?></h3>
				<p><?php the_author_meta( 'description' ); ?></p>
			</div>
		</div>
		<hr>
		<main id="main" class="site-main row grid-container test" role="main">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) :
					the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content-home', $veggee_home_page_layout );
					?>
				<?php endwhile; ?>
				<?php the_posts_navigation(); ?>
			<?php else : ?>
				<?php get_template_part( 'template-parts/content', 'none' ); ?>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php if ( ! empty( $veggee_home_page_layout ) ) { get_sidebar(); } ?>
</div><!-- .row -->

<?php get_footer(); ?>
